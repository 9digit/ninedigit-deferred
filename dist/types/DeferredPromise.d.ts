export declare class DeferredPromise<T> {
    private readonly _promise;
    private _resolve;
    private _reject;
    constructor();
    readonly promise: Promise<T>;
    resolve(value?: (T | PromiseLike<T>)): void;
    reject<T>(reason?: any): void;
}
