(function (global, factory) {
	typeof exports === 'object' && typeof module !== 'undefined' ? factory(exports) :
	typeof define === 'function' && define.amd ? define(['exports'], factory) :
	(factory((global.index = {})));
}(this, (function (exports) { 'use strict';

var DeferredPromise = (function () {
    function DeferredPromise() {
        var _this = this;
        this._promise = new Promise(function (resolve, reject) {
            _this._resolve = resolve;
            _this._reject = reject;
        });
    }
    Object.defineProperty(DeferredPromise.prototype, "promise", {
        get: function () {
            return this._promise;
        },
        enumerable: true,
        configurable: true
    });
    DeferredPromise.prototype.resolve = function (value) {
        this._resolve(value);
    };
    DeferredPromise.prototype.reject = function (reason) {
        this._reject(reason);
    };
    return DeferredPromise;
}());

exports.DeferredPromise = DeferredPromise;

Object.defineProperty(exports, '__esModule', { value: true });

})));
//# sourceMappingURL=ninedigit-deferred.umd.js.map
