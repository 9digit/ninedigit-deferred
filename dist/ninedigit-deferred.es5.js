var DeferredPromise = (function () {
    function DeferredPromise() {
        var _this = this;
        this._promise = new Promise(function (resolve, reject) {
            _this._resolve = resolve;
            _this._reject = reject;
        });
    }
    Object.defineProperty(DeferredPromise.prototype, "promise", {
        get: function () {
            return this._promise;
        },
        enumerable: true,
        configurable: true
    });
    DeferredPromise.prototype.resolve = function (value) {
        this._resolve(value);
    };
    DeferredPromise.prototype.reject = function (reason) {
        this._reject(reason);
    };
    return DeferredPromise;
}());

export { DeferredPromise };
//# sourceMappingURL=ninedigit-deferred.es5.js.map
