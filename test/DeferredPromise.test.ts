import { DeferredPromise } from '../src';

describe('DeferredPromise', () => {
  describe('promise', () => {
    it('should be instanceof Promise', () => {
      var deferred = new DeferredPromise<any>();
      expect(deferred.promise instanceof Promise).toBeTruthy();
    });
  });

  describe('resolve', () => {
    it('should return resolve value in then callback when successful', done => {
      var deferred = new DeferredPromise<boolean>();
      var resolveValue = true;

      deferred.promise
        .then(result => {
          expect(result).toEqual(resolveValue);
          done();
        })
        .catch(e => done('Test failed: ' + e));

      deferred.resolve(resolveValue);
    });
  });

  describe('reject', () => {
    it('should return error value in then callback when faulted', done => {
      var deferred = new DeferredPromise<boolean>();
      var rejectValue = new Error();

      deferred.promise
        .then(result => done(new Error('Test failed.')))
        .catch(e => {
          expect(e instanceof Error).toBeTruthy();
          done();
        });

      deferred.reject(rejectValue);
    });
  });
});
