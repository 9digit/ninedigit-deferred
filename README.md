# Deffered Promise

## Usage

### Instantiation

```javascript
var DeferredPromise = require("ninedigit-deferred").DeferredPromise;
var deferred = new DeferredPromise();

// Use like regular ES Promise
var promise = deferred.promise;

promise
    .then(value => {})
    .catch(e => {});

deferred.resolve({ userName: '111' });

// or

deferred.reject(new Error('Invalid user name.'));
```

## Build

1. Install dependencies:

    ```
    npm install
    ```

2. Run build

    ```
    npm run build
    ```

* Run tests

    ```
    npm run test
    ```